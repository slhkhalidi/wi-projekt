const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
//const bcrypt = require("bcrypt");
const User = require("./MongoDB/userModel.js");
const path = require("path");
const http = require("http");
const app = express();
const server = http.createServer(app);
const socketio = require("socket.io");
const io = socketio(server);
const nodemailer = require("nodemailer");

app.use(express.json());
app.use(cors());

/*
// Heroku Hosting //
app.use(express.static(path.join(__dirname, "/Client/build")));
app.get("*", (req, res) => {
  res.sendFile(path.join(__dirname, "Client/build", "index.html"));
});
// Heroku Hosting //
*/

mongoose
  .connect(
    "mongodb+srv://shop:1998@cluster0.c6bzk.mongodb.net/wiProjekt?retryWrites=true&w=majority"
  )
  .then(() => console.log("Database connected"))
  .catch((err) => console.log(err));

/* mongoose
    .connect(
        "mongodb://root:3456hrhr546gfdb@wip22g2database:27017/"
    )
    .then(() => console.log("Database connected"))
    .catch((err) => console.log(err)); */

server.listen(3011, () => {
  console.log("Server is Running");
});

//////////    Helpers Functions   /////////////
let users = [];
const addUser = (userId, socketId) => {
  !users.some((user) => user.userId === userId) &&
    users.push({ userId, socketId });
};

const removeUser = (socketId) => {
  users = users.filter((user) => user.socketId !== socketId);
};

const getUser = (userId) => {
  return users.find((user) => user.userId === userId);
};
//////////    Helpers Functions   /////////////

io.on("connection", (socket) => {
  // When User is connect take the user //
  socket.on("addUser", (actualUserEmail) => {
    addUser(actualUserEmail, socket.id);
    console.log(users);
    //io.emit("getUsers", users);
  });
  // When User is disconnected  remove User //
  socket.on("disconnect", () => {
    console.log("User has left !");
    removeUser(socket.id);
    io.emit("getUsers", users);
    console.log(users);
  });
  //send and get message
  socket.on("sendMessage", ({ senderId, receiverId, text }) => {
    const user = getUser(receiverId);
    if (user) {
      io.to(user.socketId).emit("getMessage", {
        senderId,
        text,
      });
    }
  });

  // send and receive Notification //
  socket.on("sendNotification", ({ userEmail }) => {
    const isUser = getUser(userEmail);
    console.log(isUser);
    if (isUser) {
      io.to(isUser.socketId).emit("receiveNotification", user.email);
    }
  });
});

// Registierung //
app.post("/registierung", async (req, res) => {
  const data = req.body;
  let newUser = null;
  try {
    //const hashedPass = bcrypt.hashSync(data.password, 5);
    newUser = new User({
      vorname: data.vorname,
      nachname: data.nachname,
      geburtsDatum: data.geburtsDatum,
      fakultaet: data.fakultaet,
      email: data.email,
      password: data.password,
      bild: null,
      interesse: data.interesse,
      abneigungen: data.abneigungen,
      role: "student",
    });
    const user = await newUser.save();
    console.log(user);
    res.status(200).send({ status: 200, message: "OK" });
  } catch (err) {
    res.status(500).send({ status: 500, message: "Duplicate object" });
    console.log(err);
  }
});

// Login //
// TODO:
// GET mit HASH Funktion nicht möglich ?
app.post("/login", async (req, res) => {
  const data = req.body;
  let userFound = null;
  //const hashedPass = bcrypt.hashSync(data.password, 5);
  try {
    if (data.email.includes("stud")) {
      userFound = await User.findOne({
        email: data.email,
        password: data.password,
      });
    } else {
      userFound = await Moderator.findOne({
        email: data.email,
        password: data.password,
      });
    }
    userFound
      ? res.status(200).send({
          status: 200,
          token: userFound,
        })
      : res.status(404).send({
          status: 404,
          message: "user not found",
        });
  } catch (err) {
    res.status(500).send({
      status: 500,
      message: "Something is wrong",
    });
    console.log(err);
  }
});

// Passwort Update //
// TODO:
// Verify are the User original or not ? //
// Encrypt + Decrypt //
app.patch("/passwortUpdate", async (req, res) => {
  const data = req.body;
  //const hashedPass = bcrypt.hashSync(data.password, 5);
  try {
    const update = await User.findOneAndUpdate(
      { email: data.email },
      { password: data.password },
      { new: true }
    );
    if (update) {
      console.log(update);
      res.status(200).send({ status: 200, message: "User Updated" });
    } else {
      res.status(404).send({ status: 404, message: "User not found" });
    }
  } catch (err) {
    res.status(500).send({ status: 500, message: "Something is wrong" });
    console.log(err);
  }
});

// Delete User //
app.delete("/deleteProfil", async (req, res) => {
  const data = req.body;
  try {
    await User.findOneAndDelete({
      email: data.email,
    });
    res.status(200).send({ status: 200, message: "Object deleted" });
  } catch (err) {
    res.status(500).send({ status: 500, message: "Something is wrong" });
    console.log(err);
  }
});

// Get matched Students //
app.post("/community", async (req, res) => {
  const actualUser = req.body.actualUser;
  try {
    const students = await User.find();
    if (students) {
      let matchedList = [];
      students.map((student) => {
        if (student.email !== actualUser.email && matchedList.length < 10) {
          for (let i = 0; i < student.interesse.length; i++) {
            if (actualUser.interesse.includes(student.interesse[i])) {
              matchedList.push(student);
              break;
            }
          }
        }
      });
      // Filter and  Check if the matched users already exists in followers //
      if (actualUser.followers.length > 0) {
        actualUser.followers.map((friend) => {
          matchedList = matchedList.filter((elem) => {
            return elem.email !== friend.email;
          });
        });
      }
      // Filter Check if the actualUser already ask to follow the matched users//
      if (actualUser.gesendeteAnfragen.length > 0) {
        actualUser.gesendeteAnfragen.map((userAsked) => {
          matchedList = matchedList.filter((match) => {
            return match.email !== userAsked.email;
          });
        });
      }
      // Filter Check if the actualUser has already received followAsk from the matched users //
      if (actualUser.receivedAnfragen.length > 0) {
        actualUser.receivedAnfragen.map((receipient) => {
          matchedList = matchedList.filter((match) => {
            return match.email !== receipient.email;
          });
        });
      }
      res.status(200).send({ status: 200, matchedStudents: matchedList });
    } else {
      res.status(404).send({ status: 404, message: "Nothing found" });
    }
  } catch (error) {
    console.log(error);
    res.status(500).send({ status: 500, message: "something is wrong" });
  }
});

// Send Friend Request //
app.patch("/community/follow", async (req, res) => {
  try {
    const data = req.body;
    const aktualUser = await User.findOneAndUpdate(
      { email: data.aktualUser.email },
      { $push: { gesendeteAnfragen: data.userAsked } },
      { new: true }
    );
    const userAsked = await User.findOneAndUpdate(
      { email: data.userAsked.email },
      { $push: { receivedAnfragen: data.aktualUser } },
      { new: true }
    );
    if (userAsked && aktualUser) {
      res.status(200).send({ status: 200, userAktualisiert: aktualUser });
    }
  } catch (err) {
    res.status(500).send({ status: 500, message: "Something is wrong" });
    console.log(err);
  }
});

// Accept a Friend Request //
app.post("/notifications/accept", async (req, res) => {
  const data = req.body;
  try {
    const actualUserUpdate = await User.findOneAndUpdate(
      { email: data.actualUser.email },
      {
        $push: {
          followers: data.newFriend,
        },
        $pull: { receivedAnfragen: data.newFriend },
      },
      { new: true }
    );
    const newNotification = {
      sender: data.actualUser.email,
      type: "newFriend",
    };
    const newFriendUpdate = await User.findOneAndUpdate(
      { email: data.newFriend.email },
      {
        $push: {
          followers: data.actualUser,
        },
      },
      { new: true }
    );
    if (actualUserUpdate && newFriendUpdate) {
      res.status(200).send({ status: 200, newUser: actualUserUpdate });
    }
  } catch (err) {
    res.status(500).send({ status: 500, message: "Something is wrong" });
    console.log(err);
  }
});

// Delete a Friend Request //
app.post("/notifications/delete", async (req, res) => {
  const data = req.body;
  try {
    const actualUserUpdate = await User.findOneAndUpdate(
      { email: data.actualUser.email },
      { $pull: { receivedAnfragen: data.newFriend } },
      { new: true }
    );
    const newFriendUpdate = await User.findOneAndUpdate(
      { email: data.newFriend.email },
      { $pull: { gesendeteAnfragen: data.actualUser } },
      { new: true }
    );
    if (actualUserUpdate && newFriendUpdate) {
      res.status(200).send({ status: 200, newUser: actualUserUpdate });
    }
  } catch (err) {
    console.log(err);
    res.status(500).send({ status: 500, message: "Something is wrong!" });
  }
});

// Chatting //
app.post("/messages", async (req, res) => {
  const data = req.body;
  try {
    // Get and Update the conversation with the Friend from DB //
    const user = await User.findOne({
      email: data.message.receiver,
    });
    let newNotifications = [...user.notifications];
    newNotifications.push({ sender: data.message.sender, type: "message" });
    const myFriend = await User.findOneAndUpdate(
      { email: data.message.receiver },
      {
        $push: {
          conversations: data.message,
        },
        $set: {
          notifications: newNotifications,
        },
      },
      { new: true }
    );
    const myUser = await User.findOneAndUpdate(
      { email: data.message.sender },
      { $push: { conversations: data.message } },
      { new: true }
    );
    if (myFriend && myUser) {
      res.status(200).send({ status: 200, newUser: myUser });
    }
  } catch (err) {
    console.log(err);
    res.status(500).send({ status: 500, message: "Something is Wring" });
  }
});

// Delete Notification //
app.delete("/deleteNotification", async (req, res) => {
  const data = req.body;
  const newNotifications = [];
  try {
    const actualUserUpdate = await User.findOneAndUpdate(
      { email: data.actualUserMail },
      { $set: { notifications: newNotifications } },
      { new: true }
    );
    if (actualUserUpdate) {
      res.status(200).send({ status: 200, newUser: actualUserUpdate });
    }
  } catch (err) {
    console.log(err);
    res.status(500).send({ status: 500, message: "Something is wrong!" });
  }
});

// Get the Current User with new Changes //
app.post("/user", async (req, res) => {
  const data = req.body;
  try {
    const user = await User.findOne({
      email: data.userMail,
    });
    if (user) {
      res.status(200).send({ newUser: user });
    }
  } catch (err) {
    res.status(404).send({ message: "User not found" });
  }
});

// Update the User //
app.patch("/profil", async (req, res) => {
  const data = req.body.newUserUpdate;
  try {
    const updateUser = await User.findOneAndUpdate(
      { email: data.email },
      {
        $set: {
          vorname: data.vorname,
          nachname: data.nachname,
          password: data.password,
          geburtsDatum: data.geburtsDatum,
          fakultaet: data.fakultaet,
          interesse: data.interessen,
          abneigungen: data.abneigungen,
        },
      },
      { new: true }
    );
    if (updateUser) {
      res.status(200).send({ status: 200, newUser: updateUser });
    }
  } catch (err) {
    res.status(500).send({ status: 500, message: err });
    console.log(err);
  }
});

// Create Event //
app.post("/neuEvent", async (req, res) => {
  const data = req.body;
  console.log(data.event);
  try {
    const moderator = await User.findOneAndUpdate(
      { email: data.actualUser },
      { $push: { publications: data.event } },
      { new: true }
    );
    if (moderator) {
      res.status(200).send({ status: 200, userAktualisiert: moderator });
    } else {
      res.status(400).send({ status: 400, message: "Something went wrong" });
    }
  } catch (err) {
    res.status(500).send({ status: 500, message: "Error" });
    console.log(err);
  }
});

// Get All Events //
app.post("/events", async (req, res) => {
  try {
    //  Get All Users from DB //
    const users = await User.find();
    //console.log(users);
    const events = [];
    users.map((user) => {
      if (user.publications.length > 0) {
        user.publications.forEach((event) => {
          events.push(event);
        });
      }
    });
    res.status(200).send({ status: 200, events: events });
  } catch (ex) {
    console.log(ex);
    res.status(500).send({ status: 500, message: "ERROR" });
  }
});

// Ask to be Moderator //
app.post("/moderator", async (req, res) => {
  try {
    const data = req.body;
    const user = data.user;
    let transporter = nodemailer.createTransport({
      service: "hotmail",
      auth: {
        user: "TODO",
        pass: "TODO",
      },
    });
    // Send Email to Admin //
    transporter.sendMail(
      {
        from: "TODO", // sender address
        to: "TODO", // list of receivers
        subject: "Neue Moderator Anfrage", // Subject line
        html: `<p>Hallo,</p>
               <p>Neue Moderator Anfrage von: ${user}</p>
               <p>Mit freundlichen Grüßen,</p>
               <p>Bamberg-connect Team.</p>`, // plain text body
      },
      (err, info) => {
        if (err) {
          console.log(err);
          res.status(500).send({ status: 500 });
        } else {
          console.log(info);
          res.status(200).send({ status: 200 });
        }
      }
    );
  } catch (err) {
    console.log(err);
  }
});

// Participate on Event //
app.post("/participateEvent", async (req, res) => {
  const data = req.body;
  const myEvent = data.eventName;
  const moderator = data.moderator;
  try {
    const user = await User.findOne({ email: moderator });
    const newPublications = [...user.publications];
    newPublications.map((event) => {
      if (event.title === myEvent) {
        event.participents.push(data.actualUser);
      }
    });
    const updateUser = await User.findOneAndUpdate(
      { email: moderator },
      { $set: { publications: newPublications } }
    );
    if (updateUser) {
      res.status(200).send({ status: 200, message: "ok" });
    }
  } catch (error) {
    res.status(500).send({ status: 500, message: "Error" });
    console.log(error);
  }
});
