import React from "react";
import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Login from "./Frontend/Login/login.js";
import RegistrierenStudent from "./Frontend/Registierung/registierungStudent.js";
import Profil from "./Frontend/Home/Profil/profil.js";
import Home from "./Frontend/Home/home";
import { Provider } from "react-redux";
import store from "./Frontend/ReduxToolkit/Store.js";
import PasswordUpdate from "./Frontend/Login/password";
import Messenger from "./Frontend/Home/Messenger/messenger.js";
import Community from "./Frontend/Home/Community/community.js";
import Notifications from "./Frontend/Home/Notifications/notifications";
import Notis from "./Frontend/Home/Notifications/notis";
import Events from "./Frontend/Home/Events/events";
import NeuEvent from "./Frontend/Home/Events/neuEvent";
import DeleteProfil from "./Frontend/Home/Profil/deleteProfile.js";
import Validator from "./Frontend/Home/Events/validator";

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/login" element={<Login />} />
          <Route path="/registierung" element={<RegistrierenStudent />} />
          <Route path="/passwortUpdate" element={<PasswordUpdate />} />
          <Route path="/profil" element={<Profil />} />
          <Route path="/messages" element={<Messenger />} />
          <Route path="/community" element={<Community />} />
          <Route path="/notis" element={<Notis />} />
          <Route path="/notifications" element={<Notifications />} />
          <Route path="/events" element={<Events />} />
          <Route path="/newEvent" element={<NeuEvent />} />
          <Route path="/deleteProfil" element={<DeleteProfil />} />
          <Route path="/validator" element={<Validator />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default function AppRedux() {
  return (
    <Provider store={store}>
      <div>
        <App />
      </div>
    </Provider>
  );
}
