const production = false;
const backend = production
  ? "http://www.web06.iis.uni-bamberg.de:3011/"
  : "http://localhost:3011/";

export default function path(endpoint) {
  return backend + endpoint;
}
