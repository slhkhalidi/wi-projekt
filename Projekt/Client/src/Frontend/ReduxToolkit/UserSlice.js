import { createSlice } from "@reduxjs/toolkit";

export const UserSlice = createSlice({
  name: "UserSlice",
  initialState: {
    actualUser: JSON.parse(localStorage.getItem("user")) || null,
    currentChat: "",
  },
  reducers: {
    setActualUser: (state, action) => {
      const user = action.payload;
      state.actualUser = { ...user };
    },
    logout: (state) => {
      state.actualUser = null;
    },
    setCurrentChat: (state, action) => {
      state.currentChat = action.payload;
    },
    deleteInteressen: (state, action) => {
      const newInteressen = [...state.actualUser.interesse];
      newInteressen.splice(action.payload, 1);
      state.actualUser.interesse = [...newInteressen];
    },
    deleteAbneigung: (state, action) => {
      const newAbneigungen = [...state.actualUser.abneigungen];
      newAbneigungen.splice(action.payload, 1);
      state.actualUser.abneigungen = [...newAbneigungen];
    },
  },
});

export const {
  setActualUser,
  logout,
  setCurrentChat,
  deleteInteressen,
  deleteAbneigung,
} = UserSlice.actions;
export default UserSlice.reducer;
