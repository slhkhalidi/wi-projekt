import { React, useState } from "react";
import "./registierungNeu.css";
import { BsInfoCircleFill } from "react-icons/bs";
import { useNavigate } from "react-router-dom";
import { Link } from "react-router-dom";
import { Multiselect } from "multiselect-react-dropdown";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import path from "../../config";

function RegistrierenStudent() {
  const navigate = useNavigate();
  const [vorname, setVorname] = useState("");
  const [nachname, setNachname] = useState("");
  const [geburtsDatum, setgeburtsDatum] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [password2, setPassword2] = useState("");
  const [fakultaet, setFakultaet] = useState("");
  const [interesse, setInteresse] = useState([]);
  const [abneigungen, setAbneigungen] = useState([]);

  function checkUniMail(mail) {
    if (mail !== "" && mail.includes("stud.uni-bamberg")) {
      setEmail(mail);
    } else {
      setEmail("");
    }
  }

  const getPassword = (pass) => {
    // if (pass.length >= 10) {
    setPassword(pass);
    // }
  };

  const getInteresse = (obj) => {
    setInteresse([...interesse, obj.Interesse]);
  };

  const getAbneigungen = (obj) => {
    setAbneigungen([...abneigungen, obj.Abneigung]);
  };

  // TODO: Validate that the Account was created and go to Home Page //
  async function register() {
    if (
      vorname !== "" &&
      nachname !== "" &&
      email !== "" &&
      password !== "" &&
      password === password2 &&
      fakultaet !== "" &&
      geburtsDatum !== "" &&
      interesse.length >= 6 &&
      abneigungen.length >= 3
    ) {
      const user = {
        vorname: vorname,
        nachname: nachname,
        geburtsDatum: geburtsDatum,
        fakultaet: fakultaet,
        email: email,
        password: password,
        interesse: interesse,
        abneigungen: abneigungen,
        bild: null,
      };
      const url = path("registierung");
      const request = await fetch(url, {
        method: "POST",
        headers: {
          "Content-type": "application/json",
        },
        body: JSON.stringify(user),
      });
      const response = await request.json();
      if (response.status === 200) {
        navigate("/login");
      } else {
        toast.error("Email oder Passwort existiert schon !", {
          position: "bottom-center",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    } else {
      toast.error("Bitte fühlen Sie alle Eingaben ein !", {
        position: "bottom-center",
        autoClose: 3000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }
  }

  const Interessen = [
    {
      Interesse: "test1",
    },
    {
      Interesse: "test2",
    },
    {
      Interesse: "test3",
    },
    ,
    {
      Interesse: "test4",
    },
    {
      Interesse: "test5",
    },
    {
      Interesse: "test6",
    },
  ];

  const Abneigungen = [
    {
      Abneigung: "test1",
    },
    {
      Abneigung: "test2",
    },
    {
      Abneigung: "test3",
    },
  ];

  return (
    <div className="container-fluid col-lg-9 p-1 persoInfos ">
      <div className="d-flex align-items-center">
        <h1 className="py-3 pl-2 mein-account">Registierung</h1>
      </div>
      <hr className="account-lign" />

      <div className="px-3 pt-3 pb-2" id="profileForm">
        <div className="form-group">
          <label className="fakultät-title">Email-Adresse:</label>
          <input
            type="email"
            className="form-control"
            placeholder="Email"
            onChange={(e) => checkUniMail(e.target.value)}
          />
        </div>
        <div className="row " id="newPasswordCollapse">
          <div className="col-sm">
            <div className="form-group">
              <label className="fakultät-title">Passwort:</label>
              <input
                type="password"
                className="form-control"
                placeholder="Neues Passwort"
                onChange={(e) => getPassword(e.target.value)}
              />
            </div>
          </div>
          <div className="col-sm">
            <div className="form-group">
              <label className="fakultät-title">Passwort bestätigen:</label>
              <input
                type="password"
                className="form-control"
                placeholder="Neues Passwort bestätigen"
                onChange={(e) => setPassword2(e.target.value)}
              />
            </div>
          </div>
        </div>

        <div className="row">
          <div className="form-group col-sm">
            <label className="fakultät-title">Vorname:</label>
            <input
              type="text"
              className="form-control"
              placeholder="Vorname"
              onChange={(e) => setVorname(e.target.value)}
            />
          </div>
          <div className="form-group col-sm">
            <label className="fakultät-title">Nachname:</label>
            <input
              type="text"
              className="form-control"
              placeholder="Nachname"
              onChange={(e) => setNachname(e.target.value)}
            />
          </div>
        </div>

        <div className="row">
          <div className="form-group col-sm">
            <span className="fakultät-title">Geburtstag:</span>
            <input
              type="date"
              className="form-control"
              onChange={(e) => setgeburtsDatum(e.target.value)}
            />
          </div>
        </div>

        <div className="fakultäten-box">
          <span className="fakultät-title">Fakultät:</span>
          <div className="fakultäten">
            <div className="fakultät form-check">
              <input
                className="form-check-input"
                type="radio"
                value="Geistes und Kulturwissenschaften"
                name="flexRadioDefault"
                id="flexRadioDefault1"
                onChange={(e) => setFakultaet(e.target.value)}
              />
              <label>Geistes und Kulturwissenschaften</label>
            </div>
            <div className="fakultät form-check">
              <input
                className="form-check-input"
                type="radio"
                value="Humanwissenschaften"
                name="flexRadioDefault"
                id="flexRadioDefault2"
                onChange={(e) => setFakultaet(e.target.value)}
              />
              <label>Humanwissenschaften</label>
            </div>
            <div className="fakultät form-check">
              <input
                className="form-check-input"
                type="radio"
                value="Sozial und Wirtschaftswissenschaften"
                name="flexRadioDefault"
                id="flexRadioDefault3"
                onChange={(e) => setFakultaet(e.target.value)}
              />
              <label>Sozial und Wirtschaftswissenschaften</label>
            </div>
            <div className="fakultät form-check">
              <input
                className="form-check-input"
                type="radio"
                value="Wirtschaftsinformatik und Angewandete Informatik"
                name="flexRadioDefault"
                id="flexRadioDefault4"
                onChange={(e) => setFakultaet(e.target.value)}
              />
              <label>Wirtschaftsinformatik und Angewandete Informatik</label>
            </div>
          </div>
        </div>

        {/* Interessen */}
        <div className="interessen-container">
          <label className="fakultät-title">Interessen:</label>
          <div className="multiselect-box">
            <Multiselect
              name="selector1"
              id="selector1"
              placeholder="Neue Interessen"
              className="selector"
              options={Interessen}
              displayValue="Interesse"
              onSelect={(selectedList, selectedItem) =>
                getInteresse(selectedItem)
              }
            />
          </div>
        </div>
        {/* Interessen */}

        {/* Abneigungen */}
        <div className="interessen-container">
          <label className="fakultät-title">Abneigungen:</label>
          <div className="multiselect-box">
            <Multiselect
              name="selector2"
              id="selector2"
              placeholder="Neue Abneigungen"
              className="selector"
              options={Abneigungen}
              displayValue="Abneigung"
              onSelect={(selectedList, selectedItem) =>
                getAbneigungen(selectedItem)
              }
            />
          </div>
        </div>
        {/* Abneigungen */}

        <div className="row pl-3 buttons-box">
          <div className="col-xs mr-1 mb-1 mt-2">
            <button
              type="button"
              className="btn purpleButton btns-profil"
              id="saveChangesButton"
              onClick={() => register()}
            >
              Registieren
            </button>
          </div>
          <div class="col-xs pr-3 mb-1 mt-2">
            <Link
              to="/login"
              type="button"
              className="btn btn-primary btns-profil"
              data-toggle="modal"
              data-target="#accountDeleteModal"
              data-backdrop="static"
              data-keyboard="false"
              id="deleteAccountButton"
            >
              Einlogen
            </Link>
          </div>
        </div>
      </div>
      <ToastContainer />
    </div>
  );
}

export default RegistrierenStudent;
