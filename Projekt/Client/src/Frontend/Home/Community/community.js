import { React, useState, useEffect, useRef } from "react";
import "./community.css";
import { useSelector, useDispatch } from "react-redux";
import { setActualUser } from "../../ReduxToolkit/UserSlice.js";
import { io } from "socket.io-client";
import { BsFillPeopleFill } from "react-icons/bs";
import path from "../../../config.js";

export default function Community() {
  const dispatch = useDispatch();
  const globalState = useSelector((state) => state);
  const actualUser = globalState.user.actualUser;
  let [matchedStudents, setmatchedStudents] = useState([]);
  const socket = useRef();

  // Get the Mathed User List from the Backend //
  async function getMatchedUsers() {
    const url = path("community");
    const request = await fetch(url, {
      method: "POST",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify({
        actualUser: actualUser,
      }),
    });
    const response = await request.json();
    if (response.status === 200) {
      const matchedList = [...response.matchedStudents];
      let listWithScore = [];
      // TODO: Score //
      matchedList.map((match) => {
        let score = 0;
        // Check Interessen Score //
        actualUser.interesse.forEach((interet) => {
          if (match.interesse.includes(interet)) {
            score = score + 1;
          }
          if (match.abneigungen.includes(interet)) {
            score = score - 1;
          }
        });
        // Check Abneigungen Score //
        actualUser.abneigungen.forEach((abneigung) => {
          if (match.interesse.includes(abneigung)) {
            score = score - 1;
          }
          if (match.abneigungen.includes(abneigung)) {
            score = score + 1;
          }
        });
        const object = {
          user: match,
          score: score,
        };
        listWithScore.push(object);
      });
      // TODO: listWithScore Sortieren > //
      listWithScore = listWithScore.sort((a, b) => b.score - a.score);
      console.log(listWithScore);

      // This Function is for Random Matching //
      // const getter = [];
      // const checker = [];
      // matchedList.map((match) => {
      //   let random = Math.floor(Math.random() * matchedList.length);
      //   if (getter.length < 10 && !checker.includes(random)) {
      //     checker.push(random);
      //     getter.push(matchedList[random]);
      //   }
      // });

      setmatchedStudents([...listWithScore]);
    } else {
      console.log(response.message);
    }
  }

  // To get Random Matching we use UseEffect and Call the Function foreach Rendering to get New 10 Matched Users //
  useEffect(() => {
    getMatchedUsers();
  }, [actualUser]);

  // Like a User //
  async function interessed(user) {
    const url = path("community/follow");
    const request = await fetch(url, {
      method: "PATCH",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify({
        aktualUser: actualUser,
        userAsked: user,
      }),
    });
    const response = await request.json();
    if (response.status === 200) {
      dispatch(setActualUser({ ...response.userAktualisiert }));
      const urlSocket = path("");
      socket.current = io(urlSocket);
      // Check ?? //
      socket.current.emit("sendNotification", { user });
    } else {
      alert("Please Try again !");
      console.log("Error");
    }
  }

  function notInteressed(student) {
    const index = matchedStudents.indexOf(student);
    let myList = [...matchedStudents];
    myList.splice(index, 1);
    setmatchedStudents([...myList]);
  }

  return (
    <div>
      <div className="community-top">
        <BsFillPeopleFill className="community-logo" />
        <h1>Community:</h1>
      </div>
      <hr className="ligne" />
      <div className="community">
        {matchedStudents.map((student) => (
          <div className="row match col-lg-9">
            <div className="col-lg-3">
              <h4 className="matching-title">Benutzer:</h4>
              <hr />
              <p>
                {student.user.vorname} {student.user.nachname}
              </p>
              <p>WIAI</p>
              <p>Matching Score: {student.score}</p>
            </div>

            <div className="col-lg-3">
              <h4 className="matching-title">Interessen:</h4>
              <hr />
              {student.user.interesse.map((intr) => (
                <p>{intr}</p>
              ))}
            </div>

            <div className="col-lg-3">
              <h4 className="matching-title">Abneigungen:</h4>
              <hr />
              {student.user.abneigungen.map((abng) => (
                <p>{abng}</p>
              ))}
            </div>

            <div className="col-lg-3">
              <h4 className="matching-title">Like ?</h4>
              <hr />
              <div className="d-grid gap-2 col-6 mx-auto">
                <button
                  onClick={() => interessed(student.user)}
                  className="btn btn-success"
                  type="button"
                >
                  Ja
                </button>
                <button
                  onClick={() => notInteressed({ student })}
                  className="btn btn-danger"
                  type="button"
                >
                  Nein
                </button>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}
