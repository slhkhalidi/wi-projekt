import { React, useEffect, useState } from "react";
import Navbar from "../Navbar/navbar";
import "./profil.css";
import { useDispatch, useSelector } from "react-redux";
import {
  logout,
  setActualUser,
  deleteInteressen,
  deleteAbneigung,
} from "../../ReduxToolkit/UserSlice.js";
import { Navigate, useNavigate } from "react-router-dom";
import { Link } from "react-router-dom";
import {
  BsFillTrashFill,
  BsFillPlusCircleFill,
  BsFillEyeSlashFill,
  BsPersonCircle,
} from "react-icons/bs";
import { Multiselect } from "multiselect-react-dropdown";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import path from "../../../config";

function Profil() {
  const globaleState = useSelector((state) => state);
  const actualUser = globaleState.user.actualUser;
  const dispatch = useDispatch();
  const navigate = useNavigate();
  let [changePass, setChangePass] = useState(false);
  let [passwordShown, setPasswordShown] = useState(false);
  const [plusI, setplusI] = useState(false);
  const [plusA, setplusA] = useState(false);
  const [changeFak, setChangeFak] = useState(false);

  // Variables to get the Changes //
  const [vorname, setVorname] = useState(actualUser.vorname);
  const [nachname, setNachname] = useState(actualUser.nachname);
  const [password, setPassword] = useState(actualUser.password);
  const [geburtsDatum, setGeburtsDatum] = useState(actualUser.geburtsDatum);
  const [fakultaet, setFakultaet] = useState(actualUser.fakultaet);
  const [interessen, setInteressen] = useState([...actualUser.interesse]);
  const [abneigungen, setAbneigungen] = useState([...actualUser.abneigungen]);

  const Interessen = [
    {
      Interesse: "test1",
    },
    {
      Interesse: "test2",
    },
    {
      Interesse: "test3",
    },
    {
      Interesse: "test4",
    },
    {
      Interesse: "test5",
    },
    {
      Interesse: "test6",
    },
  ];

  const Abneigungen = [
    {
      Abneigung: "test1",
    },
    {
      Abneigung: "test2",
    },
    {
      Abneigung: "test3",
    },
  ];
  // Active Change Passe //
  const activateChangePass = () => {
    setChangePass(!changePass);
  };

  // Interesse Löschen //
  function deleteInteresse(indx) {
    let newInteressen = [...actualUser.interesse];
    newInteressen.splice(indx, 1);
    setInteressen([...newInteressen]);
    dispatch(deleteInteressen(indx));
  }

  // Abneigungen Löschen //
  function deleteAbneigungen(indx) {
    let newAbneigungen = [...actualUser.abneigungen];
    newAbneigungen.splice(indx, 1);
    setAbneigungen([...newAbneigungen]);
    dispatch(deleteAbneigung(indx));
  }

  // Set Neue Interesse //
  function pushInteresse(obj) {
    let newInteressen = [...interessen];
    newInteressen.push(obj.Interesse);
    setInteressen([...newInteressen]);
  }

  // Set Neue Abneigungen //
  function pushAbneigung(obj) {
    let newAbneigungen = [...abneigungen];
    newAbneigungen.push(obj.Abneigung);
    setAbneigungen([...newAbneigungen]);
  }

  // Last Function to update the User //
  async function updateUser() {
    const email = actualUser.email;
    console.log(email);
    const newUserUpdate = {
      vorname: vorname,
      nachname: nachname,
      email: email,
      password: password,
      geburtsDatum: geburtsDatum,
      fakultaet: fakultaet,
      interessen: interessen,
      abneigungen: abneigungen,
    };
    const url = path("profil");
    const request = await fetch(url, {
      method: "PATCH",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify({
        newUserUpdate,
      }),
    });
    const response = await request.json();
    if (response.status === 200) {
      dispatch(setActualUser({ ...response.newUser }));
      toast.success("Änderungen erfolgreich hinzugefügt", {
        position: "bottom-center",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    } else {
      toast.error("Bitte versuchen Sie es nochmal !", {
        position: "bottom-center",
        autoClose: 3000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }
  }

  return (
    <div>
      <Navbar />
      <div className="container-fluid col-lg-9 p-1 persoInfos">
        <div className="d-flex align-items-center">
          <h1 className="py-3 pl-2 mein-account">
            <BsPersonCircle className="profil-logo" /> Mein Profil
          </h1>
        </div>
        <hr className="account-lign" />

        <div className="px-3 pt-3 pb-2" id="profileForm">
          <div className="form-group">
            <label className="fakultät-title">Email-Adresse:</label>
            <input
              type="email"
              className="form-control"
              value={actualUser.email}
              readOnly
            />
          </div>
          <div className="row">
            <div className="form-group col-sm">
              <label className="fakultät-title">Passwort:</label>
              <div className="input-group">
                <input
                  className="form-control rounded"
                  type={passwordShown === true ? "text" : "password"}
                  value={actualUser.password}
                />

                <button
                  type="button"
                  className="btn purpleButton ml-1"
                  onClick={() => setPasswordShown(!passwordShown)}
                >
                  <BsFillEyeSlashFill /> Anzeigen
                </button>
              </div>
            </div>
            <div className="form-group col-md align-self-end">
              <button
                className="btn purpleButton"
                type="button"
                onClick={() => activateChangePass()}
              >
                Passwort ändern
              </button>
            </div>
          </div>

          {changePass === true ? (
            <div className="row " id="newPasswordCollapse">
              <div className="col-sm">
                <div className="form-group">
                  <label className="fakultät-title">Neues Passwort:</label>
                  <input
                    type="password"
                    className="form-control"
                    placeholder="Neues Passwort"
                    onChange={(e) => setPassword(e.target.value)}
                  />
                </div>
              </div>
              <div className="col-sm">
                <div className="form-group">
                  <label className="fakultät-title">
                    Neues Passwort bestätigen:
                  </label>
                  <input
                    type="password"
                    className="form-control"
                    placeholder="Neues Passwort bestätigen"
                  />
                </div>
              </div>
            </div>
          ) : (
            <div></div>
          )}

          <div className="row">
            <div className="form-group col-sm">
              <label className="fakultät-title">Vorname:</label>
              <input
                type="text"
                className="form-control"
                placeholder={actualUser.vorname}
                onChange={(e) => setVorname(e.target.value)}
              />
            </div>
            <div className="form-group col-sm">
              <label className="fakultät-title">Nachname:</label>
              <input
                type="text"
                className="form-control"
                placeholder={actualUser.nachname}
                onChange={(e) => setNachname(e.target.value)}
              />
            </div>
          </div>

          <div className="row">
            <div className="form-group col-sm">
              <span className="fakultät-title">Geburtstag:</span>
              <input
                placeholder={actualUser.geburtsDatum}
                type="date"
                className="form-control"
                onChange={(e) => setGeburtsDatum(e.target.value)}
              />
            </div>
          </div>

          <div className="fakultäten-box">
            <span className="fakultät-title">Fakultät:</span>
            <div>
              <span className="meine-fakultaet">
                Deine aktuelle Fakultät ist: <b>{actualUser.fakultaet}</b>
              </span>
              <button
                onClick={() => setChangeFak(!changeFak)}
                className="btn purpleButton"
              >
                Fakultät ändern
              </button>
            </div>
            {changeFak === true ? (
              <div className="fakultäten">
                <div className="fakultät form-check">
                  <input
                    className="form-check-input"
                    type="radio"
                    value="Geistes und Kulturwissenschaften"
                    name="flexRadioDefault"
                    id="flexRadioDefault1"
                    onChange={(e) => setFakultaet(e.target.value)}
                  />
                  <label>Geistes und Kulturwissenschaften</label>
                </div>
                <div className="fakultät form-check">
                  <input
                    className="form-check-input"
                    type="radio"
                    value="Humanwissenschaften"
                    name="flexRadioDefault"
                    id="flexRadioDefault2"
                    onChange={(e) => setFakultaet(e.target.value)}
                  />
                  <label>Humanwissenschaften</label>
                </div>
                <div className="fakultät form-check">
                  <input
                    className="form-check-input"
                    type="radio"
                    value="Sozial und Wirtschaftswissenschaften"
                    name="flexRadioDefault"
                    id="flexRadioDefault3"
                    onChange={(e) => setFakultaet(e.target.value)}
                  />
                  <label>Sozial und Wirtschaftswissenschaften</label>
                </div>
                <div className="fakultät form-check">
                  <input
                    className="form-check-input"
                    type="radio"
                    value="Wirtschaftsinformatik und Angewandete Informatik"
                    name="flexRadioDefault"
                    id="flexRadioDefault4"
                    onChange={(e) => setFakultaet(e.target.value)}
                  />
                  <label>
                    Wirtschaftsinformatik und Angewandete Informatik
                  </label>
                </div>
              </div>
            ) : (
              <div></div>
            )}
          </div>

          {/* Interessen */}
          <div className="interessen-container">
            <label className="fakultät-title">Interessen:</label>
            <div className="interessen-box">
              {actualUser.interesse.map((interet, index) => (
                <div className="interet">
                  <span className="interet-title">{interet}</span>
                  <button
                    className="btn btn-danger delete"
                    onClick={() => deleteInteresse(index)}
                  >
                    <BsFillTrashFill />
                  </button>
                </div>
              ))}
              <BsFillPlusCircleFill
                onClick={() => setplusI(!plusI)}
                className="plus"
              />
            </div>
            {plusI === true ? (
              <div className="multiselect-box">
                <Multiselect
                  name="selector1"
                  id="selector1"
                  placeholder="Neue Interessen"
                  className="selector"
                  options={Interessen}
                  displayValue="Interesse"
                  onSelect={(selectedList, selectedItem) =>
                    pushInteresse(selectedItem)
                  }
                />
              </div>
            ) : (
              <div></div>
            )}
          </div>
          {/* Interessen */}

          {/* Abneigungen */}
          <div className="interessen-container">
            <label className="fakultät-title">Abneigungen:</label>
            <div className="interessen-box">
              {actualUser.abneigungen.map((abneigung, index) => (
                <div className="interet">
                  <span className="interet-title">{abneigung}</span>
                  <button
                    className="btn btn-danger delete"
                    onClick={() => deleteAbneigungen(index)}
                  >
                    <BsFillTrashFill />
                  </button>
                </div>
              ))}
              <BsFillPlusCircleFill
                onClick={() => setplusA(!plusA)}
                className="plus"
              />
            </div>
            {plusA === true ? (
              <div className="multiselect-box">
                <Multiselect
                  name="selector2"
                  id="selector2"
                  placeholder="Neue Abneigungen"
                  className="selector"
                  options={Abneigungen}
                  displayValue="Abneigung"
                  onSelect={(selectedList, selectedItem) =>
                    pushAbneigung(selectedItem)
                  }
                />
              </div>
            ) : (
              <div></div>
            )}
          </div>
          {/* Abneigungen */}

          <div className="row pl-3 buttons-box">
            <div className="col-xs mr-1 mb-1 mt-2">
              <button
                onClick={() => updateUser()}
                type="button"
                className="btn purpleButton btns-profil"
                id="saveChangesButton"
              >
                Änderungen speichern
              </button>
            </div>
            <div class="col-xs pr-3 mb-1 mt-2">
              <Link
                to="/deleteProfil"
                type="button"
                className="btn btn-danger btns-profil"
                data-toggle="modal"
                data-target="#accountDeleteModal"
                data-backdrop="static"
                data-keyboard="false"
                id="deleteAccountButton"
              >
                Account löschen
              </Link>
            </div>
            <ToastContainer />
          </div>
        </div>
      </div>
    </div>
  );
}

export default Profil;
