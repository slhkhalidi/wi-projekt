import React from "react";
import "./profil.css";
import { Link, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { logout, setActualUser } from "../../ReduxToolkit/UserSlice.js";
import path from "../../../config";

export default function DeleteProfil() {
  const globaleState = useSelector((state) => state);
  const actualUser = globaleState.user.actualUser;
  const dispatch = useDispatch();
  const navigate = useNavigate();

  // TODO: remove from all Lists //
  async function deleteAccount() {
    const url = path("deleteProfil");
    const request = await fetch(url, {
      method: "DELETE",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify({
        email: actualUser.email,
      }),
    });
    const response = await request.json();
    if (response.status === 200) {
      dispatch(logout());
      localStorage.setItem("user", JSON.stringify(null));
      navigate("/");
      console.log(globaleState.user.actualUser);
    } else {
      alert("Please Try again");
    }
  }

  return (
    <div className="delete-account-container">
      <div className="delete-account">
        <h2 className="delete-title">Account löschen</h2>
        <span>
          Sind Sie sich sicher, dass Sie den Account unwiderruflich löschen
          wollen?
        </span>
        <div className="btns-box">
          <Link to="/" className="btn btn-primary abbrechen">
            Abbrechen
          </Link>
          <button
            onClick={() => deleteAccount()}
            className="btn btn-danger abbrechen"
          >
            Account löschen
          </button>
        </div>
      </div>
    </div>
  );
}
