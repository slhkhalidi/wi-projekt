import { useEffect, useState, useRef } from "react";
import user from "../user.png";
import "./chat.css";
import { useSelector, useDispatch } from "react-redux";
import Messages from "../Messages/messages";
import { setCurrentChat } from "../../../ReduxToolkit/UserSlice";
import { BsFillPeopleFill } from "react-icons/bs";

export default function Chat() {
  const globalState = useSelector((state) => state);
  const actualUser = globalState.user.actualUser;
  const currentChat = globalState.user.currentChat;
  const dispatch = useDispatch();

  return (
    <div>
      <div className="messenger">
        <div className="freunden">
          <div className="freundenWrapper">
            <div className="freunden-title-box">
              <h4 className="freunden-title">
                <BsFillPeopleFill className="freunden-icon" />
                Freunden:
              </h4>
            </div>
            <hr />
            {actualUser.followers.map((follower) => (
              <div
                onClick={() => dispatch(setCurrentChat(follower))}
                className="friend"
              >
                <img className="avatar" src={user} alt="#" />
                <span className="avatar-name">
                  {follower.vorname} {follower.nachname}
                </span>
              </div>
            ))}
          </div>
        </div>
        <Messages />
      </div>
    </div>
  );
}
