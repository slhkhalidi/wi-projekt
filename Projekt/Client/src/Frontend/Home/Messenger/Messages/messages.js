import { React, useEffect, useState, useRef } from "react";
import "./messages.css";
import { useSelector, useDispatch } from "react-redux";
import { io } from "socket.io-client";
import { setActualUser } from "../../../ReduxToolkit/UserSlice.js";
import user from "../user.png";
import { BsFillCursorFill } from "react-icons/bs";
import path from "../../../../config.js";

function Messages() {
  const globalState = useSelector((state) => state);
  const currentChat = globalState.user.currentChat;
  const actualUser = globalState.user.actualUser;
  const dispatch = useDispatch();
  const [message, setMessage] = useState("");
  const scrollRef = useRef(null);
  const socket = useRef();
  const inputRef = useRef();

  useEffect(() => {
    scrollRef.current?.scrollIntoView({
      behavior: "smooth",
    });
  }, [actualUser, currentChat]);

  const test = () => {
    console.log(currentChat);
  };

  useEffect(() => {
    const urlSocket = path("");
    socket.current = io(urlSocket);
    socket.current.emit("addUser", actualUser.email);
    socket.current.on("getMessage", (data) => {
      const receiveNewMessage = async () => {
        const url = path("user");
        const request = await fetch(url, {
          method: "POST",
          headers: {
            "Content-type": "application/json",
          },
          body: JSON.stringify({
            userMail: actualUser.email,
          }),
        });
        const response = await request.json();
        dispatch(setActualUser({ ...response.newUser }));
      };
      receiveNewMessage();
    });
  }, []);

  async function sendMessage() {
    const messageTosend = {
      sender: actualUser.email,
      receiver: currentChat.email,
      text: message,
    };
    const url = path("messages");
    const request = await fetch(url, {
      method: "POST",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify({
        message: messageTosend,
      }),
    });
    const response = await request.json();
    if (response.status === 200) {
      socket.current.emit("sendMessage", {
        senderId: actualUser.email,
        receiverId: currentChat.email,
        text: message,
      });
      dispatch(setActualUser({ ...response.newUser }));
    }
    setMessage("");
  }

  return (
    <div className="chatBox">
      {currentChat === "" ? (
        <div>Wähle eine Freund um zu unterhalten</div>
      ) : (
        <div className="chatBoxWrapper">
          <div className="chatBoxTop">
            <div className="chat-name">
              <img className="avatar" src={user} alt="#" />
              <h2 className="current-chat-name">
                {currentChat.vorname} {currentChat.nachname}
              </h2>
            </div>
            <hr className="ligne" />
            <div>
              {actualUser.conversations.map((message) => {
                if (
                  (message.sender === currentChat.email &&
                    message.receiver === actualUser.email) ||
                  (message.sender === actualUser.email &&
                    message.receiver === currentChat.email)
                ) {
                  return (
                    <div
                      ref={scrollRef}
                      className={
                        message.sender === actualUser.email
                          ? "message own"
                          : "message"
                      }
                    >
                      <div className="messageBox">
                        <p className="messageText">{message.text}</p>
                      </div>
                    </div>
                  );
                }
              })}
            </div>
          </div>
          <div className="chatBoxBottom">
            <textarea
              onChange={(e) => setMessage(e.target.value)}
              className="chatMessageInput"
              value={message}
            ></textarea>
            <button onClick={() => sendMessage()} className="chatSubmitButton">
              <BsFillCursorFill className="sendIcon" />
              Send
            </button>
          </div>
        </div>
      )}
    </div>
  );
}

export default Messages;
