import { React } from "react";
import Navbar from "../Navbar/navbar";
import Chat from "./Chat/chat.js";

function Messenger() {
  return (
    <div>
      <Navbar />
      <Chat />
    </div>
  );
}

export default Messenger;
