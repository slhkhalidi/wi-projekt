import React, { useState, useEffect } from "react";
import "./events.css";
import { useSelector, useDispatch } from "react-redux";
import Navbar from "../Navbar/navbar";
import AllEvents from "./AllEvents.js";

export default function Events() {
  const globaleState = useSelector((state) => state);
  const actualUser = globaleState.user.actualUser;

  return (
    <div>
      <Navbar />
      <div>
        <AllEvents />
      </div>
    </div>
  );
}
