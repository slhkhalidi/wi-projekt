import React, { useState, useEffect } from "react";
import "./events.css";
import { useSelector, useDispatch } from "react-redux";
import { setActualUser } from "../../ReduxToolkit/UserSlice.js";
import Navbar from "../Navbar/navbar";
import { Link, useNavigate } from "react-router-dom";
import { BsFillExclamationTriangleFill } from "react-icons/bs";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { BsFillCalendarPlusFill } from "react-icons/bs";
import path from "../../../config.js";

export default function NeuEvent() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const globaleState = useSelector((state) => state);
  const actualUser = globaleState.user.actualUser;
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [date, setDate] = useState("");
  const [time, setTime] = useState("");
  const [place, setPlace] = useState("");

  // Create a New Event //
  async function createEvent() {
    if (
      title === "" ||
      description === "" ||
      date === "" ||
      time === "" ||
      place === ""
    ) {
      alert("Bitte alle Informationen einfüllen");
    } else {
      const event = {
        title: title,
        description: description,
        date: date,
        time: time,
        place: place,
        participents: [],
        moderator: actualUser,
      };
      const url = path("neuEvent");
      const request = await fetch(url, {
        method: "POST",
        headers: {
          "Content-type": "application/json",
        },
        body: JSON.stringify({
          actualUser: actualUser.email,
          event: event,
        }),
      });
      const response = await request.json();
      if (response.status === 200) {
        dispatch(setActualUser({ ...response.userAktualisiert }));
        navigate("/events");
      } else {
        alert("Bitte versuchen Sie es nochmal");
      }
    }
  }

  // Ask to be a Moderator //
  const beModerator = async () => {
    const url = path("moderator");
    const request = await fetch(url, {
      method: "POST",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify({
        user: actualUser.email,
      }),
    });
    const response = await request.json();
    if (response.status === 200) {
      navigate("/validator");
    }
  };

  return (
    <div>
      <Navbar />
      {actualUser.role === "moderator" ? (
        <div className="events-body">
          <div className="neueevent-container">
            <div className="calenderBox">
              <div>
                <BsFillCalendarPlusFill className="calenderLogo" />
              </div>
              <h3>Event erstellen:</h3>
            </div>
            <div className="event-column">
              <div>
                <span>Title:</span>
                <input
                  className="newEventInfos"
                  type="text"
                  onChange={(e) => setTitle(e.target.value)}
                />
              </div>
              <div>
                <span>Beschreibung:</span>
                <textarea
                  class="form-control"
                  id="exampleFormControlTextarea1"
                  rows="2"
                  onChange={(e) => setDescription(e.target.value)}
                ></textarea>
              </div>

              <div>
                <span>Start Date:</span>
                <input type="date" onChange={(e) => setDate(e.target.value)} />
              </div>

              <div>
                <span>Ende Date:</span>
                <input type="date" onChange={(e) => setDate(e.target.value)} />
              </div>

              <div>
                <span> Uhrzeit:</span>
                <input type="time" onChange={(e) => setTime(e.target.value)} />
              </div>

              <div>
                <span>Ort:</span>
                <input type="text" onChange={(e) => setPlace(e.target.value)} />
              </div>
            </div>

            <div className="neuEvent-Btns">
              <button
                onClick={() => createEvent()}
                className="btn btn-success erstellenBtn"
              >
                Erstellen
              </button>
              <Link to="/events" className="btn btn-primary erstellenBtn">
                Zurück
              </Link>
            </div>
          </div>
        </div>
      ) : (
        <div className="moderatorRolle">
          <BsFillExclamationTriangleFill className="alerta" />
          <p>
            Sie sind kein Moderator, um einen neuen Event zu erstellen Sie
            brauchen den Moderator Rolle, bitte fragen Sie ihre Admin um den
            Moderator Rolle zu haben.
          </p>
          <button
            onClick={() => beModerator()}
            className="btn btn-primary askModeratorBtn"
          >
            Moderator Rolle nachfragen
          </button>
          <ToastContainer />
        </div>
      )}
    </div>
  );
}
