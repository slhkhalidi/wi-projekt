import React from "react";
import "./events.css";
import Navbar from "../Navbar/navbar";
import { useNavigate } from "react-router-dom";
import { BsCheckCircleFill } from "react-icons/bs";

export default function Validator() {
  const navigate = useNavigate();
  return (
    <div>
      <Navbar />
      <div className="moderatorRolle">
        <BsCheckCircleFill className="validator" />
        <p>
          Sie haben erfolgreich einen Moderator Rolle Anfrage geschickt, Sie
          bekommen eine Bestätigung sobald der Admin ihre Anfrage validiert.
        </p>
        <button
          onClick={() => navigate("/events")}
          className="btn btn-primary askModeratorBtn"
        >
          Zurück zum Events
        </button>
        <div />
      </div>
    </div>
  );
}
