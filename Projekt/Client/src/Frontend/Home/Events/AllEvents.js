import { React, useEffect, useState } from "react";
import "./Allevents.css";
import {
  BsAlarmFill,
  BsCalendarCheckFill,
  BsGeoAltFill,
  BsXCircleFill,
  BsCheckCircleFill,
  BsCalendar3,
  BsFillCalendarPlusFill,
} from "react-icons/bs";
import { Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { setActualUser } from "../../ReduxToolkit/UserSlice.js";
import path from "../../../config.js";

export default function AllEvents() {
  const dispatch = useDispatch();
  const globaleState = useSelector((state) => state);
  const actualUser = globaleState.user.actualUser;
  const [events, setEvents] = useState([]);

  // TODO: Get all Events and Map them in the DIV //
  async function getAllEvents() {
    const url = path("events");
    const request = await fetch(url, {
      method: "POST",
      headers: {
        "Content-type": "application/json",
      },
    });
    const response = await request.json();
    if (response.status === 200) {
      const eventsToken = [...response.events];
      setEvents([...eventsToken]);
    }
  }

  // UseEffect to get all events from Backend //
  useEffect(() => {
    getAllEvents();
  }, []);

  // Participate on Event //
  const participate = async (eventName, moderator) => {
    const url = path("participateEvent");
    const request = await fetch(url, {
      method: "POST",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify({
        actualUser: actualUser.email,
        eventName: eventName,
        moderator: moderator,
      }),
    });
    const response = await request.json();
    if (response.status === 200) {
      getAllEvents();
    } else {
      alert("Bitte versuchen Sie es nochmal");
    }
  };

  // Not interresed on Event //
  function notInteressed(index) {
    let myList = [...events];
    myList.splice(index, 1);
    setEvents([...myList]);
  }

  return (
    <div>
      <div className="events-top">
        <div className="eventTop-left">
          <BsCalendar3 className="community-logo" />
          <h1>Events:</h1>
        </div>
        <div className="eventTop-right">
          <Link className="btn btn-primary" to="/newEvent">
            <BsFillCalendarPlusFill className="neueEvent-icon" />
            Neue Event erstellen
          </Link>
        </div>
      </div>
      <hr className="ligne" />

      {events.map((event, index) => (
        <div className="events-container">
          <div className="row match col-lg-9 event">
            <div className="col-lg-3">
              <h4 className="matching-title">Event:</h4>
              <hr />
              <p>
                <BsCalendar3 className="event-icon" />
                {event.title}
              </p>
              <p>
                <BsCalendarCheckFill className="event-icon" /> {event.date}
              </p>
              <p>
                <BsAlarmFill className="event-icon" />
                {event.time} Uhr
              </p>
              <p>
                <BsGeoAltFill className="event-icon" />
                {event.place}
              </p>
              <p>
                Moderator: {event.moderator.vorname} {event.moderator.nachname}
              </p>
            </div>

            <div className="col-lg-3">
              <h4 className="matching-title">Beschreibung</h4>
              <hr />
              <p>{event.description}</p>
            </div>

            <div className="col-lg-3">
              <h4 className="matching-title">Teilnehemer:</h4>
              <hr />
              <p>{event.participents.length} Studenten</p>
            </div>

            <div className="col-lg-3">
              <h4 className="matching-title">Teilnehmen ?</h4>
              <hr />
              {event.participents.includes(actualUser.email) ? (
                <div>
                  <p>Du bist dabei !</p>
                </div>
              ) : (
                <div className="d-grid gap-2 col-6 mx-auto">
                  <button
                    onClick={() =>
                      participate(event.title, event.moderator.email)
                    }
                    className="btn btn-success"
                    type="button"
                  >
                    <BsCheckCircleFill />
                  </button>
                  <button
                    onClick={() => notInteressed(index)}
                    className="btn btn-danger"
                    type="button"
                  >
                    <BsXCircleFill />
                  </button>
                </div>
              )}
            </div>
          </div>
        </div>
      ))}
    </div>
  );
}
