import React, { useEffect } from "react";
import NewLogin from "../Login/newLogin.js";
import { useSelector } from "react-redux";
import Navbar from "./Navbar/navbar";
import Community from "../Home/Community/community.js";
function Home() {
  const globaleState = useSelector((state) => state);
  const actualUser = globaleState.user.actualUser;

  return (
    <div>
      {actualUser === null ? (
        <NewLogin />
      ) : (
        <div>
          <Navbar />
          <Community />
        </div>
      )}
    </div>
  );
}

export default Home;
