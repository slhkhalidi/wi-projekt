import { React, useEffect, useState, useRef } from "react";
import Navbar from "../Navbar/navbar";
import { useSelector, useDispatch } from "react-redux";
import { setActualUser } from "../../ReduxToolkit/UserSlice";
import { io } from "socket.io-client";
import "../Community/community.css";
import { Link } from "react-router-dom";
import "./notifications.css";
import { BsFillInfoCircleFill, BsFillPersonPlusFill } from "react-icons/bs";
import path from "../../../config.js";

function Notifications() {
  const dispatch = useDispatch();
  const globalState = useSelector((state) => state);
  const actualUser = globalState.user.actualUser;
  const socket = useRef();

  // Ja //
  async function anfrageAkzeptieren(newFriend) {
    const url = path("notifications/accept");
    const request = await fetch(url, {
      method: "POST",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify({
        actualUser: actualUser,
        newFriend: newFriend,
      }),
    });
    const response = await request.json();
    if (response.status === 200) {
      dispatch(setActualUser({ ...response.newUser }));
      const pathSocket = path("");
      socket.current = io(pathSocket);
      socket.current.emit("sendNotification", { user: newFriend });
    } else {
      console.log(response.message);
    }
  }

  // NEIN //
  async function anfrageLöschen(newFriend) {
    const url = path("notifications/delete");
    const request = await fetch(url, {
      method: "POST",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify({
        actualUser: actualUser,
        newFriend: newFriend,
      }),
    });
    const response = await request.json();
    if (response.status === 200) {
      dispatch(setActualUser({ ...response.newUser }));
    }
  }

  return (
    <div>
      <Navbar />
      <div className="community-top">
        <BsFillPersonPlusFill className="community-logo" />
        <h1>Freundschaftsanfragen:</h1>
      </div>
      <hr className="ligne" />
      <div className="community">
        {actualUser.receivedAnfragen.length > 0 ? (
          actualUser.receivedAnfragen.map((student) => (
            <div className="row match col-lg-9 ">
              <div className="col-lg-3">
                <h4 className="matching-title">Benutzer:</h4>
                <hr />
                <p>
                  {student.vorname} {student.nachname}
                </p>
                <p>WIAI</p>
              </div>

              <div className="col-lg-3">
                <h4 className="matching-title">Interessen:</h4>
                <hr />
                {student.interesse.map((intr) => (
                  <p>{intr}</p>
                ))}
              </div>

              <div className="col-lg-3">
                <h4 className="matching-title">Abneigungen:</h4>
                <hr />
                {student.abneigungen.map((abng) => (
                  <p>{abng}</p>
                ))}
              </div>

              <div className="col-lg-3">
                <h4 className="matching-title">Like ?</h4>
                <hr />
                <div className="d-grid gap-2 col-6 mx-auto">
                  <button
                    onClick={() => anfrageAkzeptieren(student)}
                    className="btn btn-success"
                    type="button"
                  >
                    Ja
                  </button>
                  <button
                    onClick={() => anfrageLöschen(student)}
                    className="btn btn-danger"
                    type="button"
                  >
                    Nein
                  </button>
                </div>
              </div>
            </div>
          ))
        ) : (
          <div className="nothing">
            <span className="goToMatchTitle">
              <BsFillInfoCircleFill className="info-icon" />
              Du hast bisher keine Freundschaftsanfragen, such dir neue Freunden
            </span>
            <Link to="/" className="btn btn-primary">
              Freunde Suchen
            </Link>
          </div>
        )}
      </div>
    </div>
  );
}

export default Notifications;
