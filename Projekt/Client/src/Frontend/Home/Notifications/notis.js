import { React, useEffect, useState } from "react";
import Navbar from "../Navbar/navbar";
import { useSelector, useDispatch } from "react-redux";
import { setActualUser } from "../../ReduxToolkit/UserSlice";
import { useNavigate } from "react-router-dom";
import { setCurrentChat } from "../../ReduxToolkit/UserSlice";

function Notis() {
  const dispatch = useDispatch();
  const globalState = useSelector((state) => state);
  const actualUser = globalState.user.actualUser;
  const navigate = useNavigate();

  // on click setCurrentChat === noti //
  const gotoMessage = (freundMail) => {
    dispatch(setCurrentChat(freundMail));
    navigate("/messages");
  };

  const deleteNotification = async (notification) => {
    const request = await fetch("http://localhost:3001/deleteNotification", {
      method: "DELETE",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify({
        actualUserMail: actualUser.email,
        notification: notification,
      }),
    });
    const response = await request.json();
    if (response.status === 200) {
      dispatch(setActualUser({ ...response.newUser }));
    }
  };

  return (
    <div>
      <Navbar />
      <div className="community">
        <h4>Notifications:</h4>
        {actualUser.notifications.map((noti) =>
          actualUser.followers.map((follower) => {
            if (follower.email === noti.sender && noti.type === "message") {
              return (
                <div>
                  <span>
                    {follower.vorname} {follower.nachname} hat dir ein neu
                    Nachricht geschickt
                  </span>
                  <button
                    onClick={() => gotoMessage(follower.email)}
                    className="btn btn-primary"
                  >
                    Nachricht lesen
                  </button>
                  <button
                    onClick={() => deleteNotification(noti)}
                    className="btn btn-danger"
                  >
                    löschen
                  </button>
                </div>
              );
            } else if (
              follower.email === noti.sender &&
              noti.type === "newFriend"
            ) {
              return (
                <div>
                  <span>
                    {follower.vorname} {follower.nachname} hat deine
                    Freundschaftsanfrage akzeptiert
                  </span>
                  <button
                    onClick={() => deleteNotification(noti)}
                    className="btn btn-danger"
                  >
                    gelesen
                  </button>
                </div>
              );
            }
          })
        )}
      </div>
    </div>
  );
}

export default Notis;
