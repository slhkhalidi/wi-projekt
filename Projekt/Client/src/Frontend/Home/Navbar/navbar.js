import { React, useEffect, useState, useRef } from "react";
import "./navbar.css";
import { Link, useNavigate } from "react-router-dom";
import logo from "../../images/logo.png";
import {
  BsChatFill,
  BsCalendar3,
  BsFillPeopleFill,
  BsFillGearFill,
} from "react-icons/bs";
import { useSelector, useDispatch } from "react-redux";
import { logout } from "../../ReduxToolkit/UserSlice.js";
import { io } from "socket.io-client";
import { setActualUser } from "../../ReduxToolkit/UserSlice";
import path from "../../../config";

function Navbar() {
  const globaleState = useSelector((state) => state);
  const actualUser = globaleState.user.actualUser;
  const currentChat = globaleState.user.currentChat;
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const socket = useRef();

  useEffect(() => {
    let myUser = globaleState.user.actualUser;
    localStorage.setItem("user", JSON.stringify(myUser));
  }, [globaleState.user.actualUser]);

  function logOut() {
    dispatch(logout());
    localStorage.setItem("user", JSON.stringify(null));
    navigate("/");
  }

  useEffect(() => {
    const urlSocket = path("");
    socket.current = io(urlSocket);
    socket.current.emit("addUser", actualUser.email);
    socket.current.on("getMessage", (data) => {
      console.log("message is here");
      // check current Chat //
      if (currentChat !== data.senderId) {
        // Fetch and get updated User //
        const url = path("user");
        const receiveNewMessage = async () => {
          const request = await fetch(url, {
            method: "POST",
            headers: {
              "Content-type": "application/json",
            },
            body: JSON.stringify({
              userMail: actualUser.email,
            }),
          });
          const response = await request.json();
          dispatch(setActualUser({ ...response.newUser }));
        };
        receiveNewMessage();
      }
    });
    socket.current.on("receiveNotification", () => {
      console.log("notification is here");
      const receiveNewNotification = async (data) => {
        const userUrl = path("user");
        const request = await fetch(userUrl, {
          method: "POST",
          headers: {
            "Content-type": "application/json",
          },
          body: JSON.stringify({
            userMail: actualUser.email,
          }),
        });
        const response = await request.json();
        dispatch(setActualUser({ ...response.newUser }));
      };
      receiveNewNotification();
    });
  }, []);

  async function deleteNotifications() {
    const url = path("deleteNotification");
    const request = await fetch(url, {
      method: "DELETE",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify({
        actualUserMail: actualUser.email,
      }),
    });
    const response = await request.json();
    if (response.status === 200) {
      dispatch(setActualUser({ ...response.newUser }));
    }
  }

  const goToMessages = () => {
    deleteNotifications();
    navigate("/messages");
  };

  return (
    <div>
      <nav className="Navbar">
        <div className="logo-box" onClick={() => navigate("/")}>
          <img src={logo} alt="#" className="logo fleur" />
        </div>
        <div className="things">
          <div>
            <span className="span">{actualUser.receivedAnfragen.length}</span>
            <BsFillPeopleFill
              onClick={() => navigate("/notifications")}
              className="icons"
            />
          </div>
          <BsCalendar3 onClick={() => navigate("/events")} className="icons" />
          <div>
            <span className="span">{actualUser.notifications.length}</span>
            <BsChatFill onClick={() => goToMessages()} className="icons" />
          </div>
          <BsFillGearFill
            onClick={() => navigate("/profil")}
            className="icons"
          />
          <button
            onClick={() => logOut()}
            type="button"
            className="btn btn-danger logout"
          >
            Logout
          </button>
        </div>
      </nav>
    </div>
  );
}

export default Navbar;
