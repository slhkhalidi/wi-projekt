import { React, useState } from "react";
import { useNavigate } from "react-router-dom";
import "./login.css";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import path from "../../config.js";

function PasswordUpdate() {
  const [email, setEmail] = useState("");
  const [password1, setPassord1] = useState("");
  const [password2, setPassord2] = useState("");
  const navigate = useNavigate();

  async function updatePassword() {
    if (email !== "" && password1 !== "" && password1 === password2) {
      const url = path("passwortUpdate");
      const request = await fetch(url, {
        method: "PATCH",
        headers: {
          "Content-type": "application/json",
        },
        body: JSON.stringify({
          email: email,
          password: password1,
        }),
      });
      const response = await request.json();
      if (response.status === 200) {
        navigate("/");
      } else {
        toast.error("Email ist falsch !", {
          position: "bottom-center",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    } else {
      toast.error("Die Eingaben sind nicht korrekt !", {
        position: "bottom-center",
        autoClose: 3000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }
  }
  return (
    <div className="login">
      <div className="loginWrapper">
        <div className="loginLeft">
          <h3 className="login-titel">Bamberg-Connect</h3>
          <span className="loginDesc">
            Verbinde dich mit Studenten der Universität Bamberg.
          </span>
        </div>
        <div className="newPass-box">
          <div className="newPass-title">
            <h3>Passwort Ändern</h3>
          </div>

          <input
            onChange={(e) => setEmail(e.target.value)}
            type="email"
            placeholder="Email"
            className="newPassInfos"
          />
          <input
            onChange={(e) => setPassord1(e.target.value)}
            type="password"
            placeholder="Neue Passwort"
            className="newPassInfos"
          />
          <input
            onChange={(e) => setPassord2(e.target.value)}
            type="password"
            placeholder="Neue Passwort bestätigen"
            className="newPassInfos"
          />
          <button onClick={() => updatePassword()} className="newPass-btns">
            Passwort ändern
          </button>
          <button onClick={() => navigate("/login")} className="newPass-btns">
            Log into Account
          </button>
        </div>
        <ToastContainer />
      </div>
    </div>
  );
}

export default PasswordUpdate;
