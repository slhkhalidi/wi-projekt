import { React, useState, useEffect, useRef } from "react";
import { Link, useNavigate } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { setActualUser } from "../ReduxToolkit/UserSlice";
import { io } from "socket.io-client";
import "./login.css";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import path from "../../config.js";

function Login() {
  const globaleState = useSelector((state) => state);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [email, setEmail] = useState("");
  const [pass, setPass] = useState("");
  const socket = useRef();

  useEffect(() => {
    let myUser = globaleState.user.actualUser;
    localStorage.setItem("user", JSON.stringify(myUser));
  }, [globaleState.user.actualUser]);

  async function validator(mail, pass) {
    if (mail !== "" && pass !== "") {
      const request = await fetch(
          path("login"),
        {
          method: "POST",
          headers: {
            "Content-type": "application/json",
          },
          body: JSON.stringify({
            email: mail,
            password: pass,
          }),
        }
      );
      const response = await request.json();
      if (response.status === 200) {
        const user = response.token;
        dispatch(setActualUser(user));
        navigate("/");
      } else {
        toast.error("Email oder Passwort ist falsch !", {
          position: "bottom-center",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    } else {
      toast.error("Bitte Email und Passwort eingeben !", {
        position: "bottom-center",
        autoClose: 3000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }
  }

  return (
    <div className="login">
      <div className="loginWrapper">
        <div className="loginLeft">
          <h3 className="login-titel">Bamberg-Connect</h3>
          <span className="loginDesc">
            Verbinde dich mit Studenten der Uni Bamberg.
          </span>
        </div>
        <div className="loginRight">
          <div className="login-box">
            <input
              type="email"
              placeholder="Email"
              className="loginInput"
              onChange={(event) => setEmail(event.target.value)}
            />
            <input
              type="password"
              placeholder="Password"
              className="loginInput"
              onChange={(event) => setPass(event.target.value)}
            />
            <button
              className="loginButton"
              onClick={() => validator(email, pass)}
            >
              Login
            </button>
            <Link to="/passwortUpdate" className="login-forgot">
              Passwort vergessen ?
            </Link>
            <button
              onClick={() => navigate("/registierung")}
              className="login-registerButton"
            >
              Registrieren
            </button>
          </div>
        </div>
      </div>
      <ToastContainer />
    </div>
  );
}

export default Login;
