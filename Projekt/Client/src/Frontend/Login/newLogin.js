import React from "react";
import "./newLogin.css";
import logo from "../images/logo.png";
import students from "../images/student.png";
import bamberg from "../images/bamberg.jpg";
import {
  BsFillGeoAltFill,
  BsFillPeopleFill,
  BsCalendarWeekFill,
  BsFacebook,
  BsInstagram,
  BsTwitter,
  BsYoutube,
} from "react-icons/bs";
import { Link } from "react-router-dom";

function NewLogin() {
  return (
    <div>
      <section className="colored-section" id="title">
        <div className="container-fluid">
          <nav className="navbar navbar-expand-lg navbar-dark">
            <div>
              <img className="logo" src={logo} alt="#" />
            </div>
            <div className="collapse navbar-collapse" id="navbarTogglerDemo02">
              <ul className="navbar-nav ms-auto">
                <li className="nav-item">
                  <Link className="nav-link" to="/login">
                    Login
                  </Link>
                </li>
                <li className="nav-item">
                  <Link to="/registierung" className="nav-link">
                    Registrierung
                  </Link>
                </li>

                <li className="nav-item">
                  <a className="nav-link" href="#footer">
                    Kontakt
                  </a>
                </li>
              </ul>
            </div>
          </nav>

          <div className="row">
            <div className="col-lg-6">
              <h1 className="big-heading">
                Treffe neue Leute von der Uni Bamberg
              </h1>
              <a href="Registrierung.html">
                <Link
                  className=" download-butten btn btn-primary btn-lg "
                  type="button"
                  to="/registierung"
                >
                  Registrieren
                </Link>
              </a>

              <a href="Login.html">
                <Link
                  to="/login"
                  className=" download-butten btn btn-outline-light btn-lg "
                  type="button"
                >
                  Einloggen
                </Link>
              </a>
            </div>

            <div className="col-lg-6">
              <img className="tittel-img" src={students} alt="iphone-mockup" />
            </div>
          </div>
        </div>
      </section>
      <div>
        <img className="bamberg" src={bamberg} />
      </div>
      <section className="white-section" id="features">
        <div className="container-fluid">
          <div className="row">
            <div className="col-lg-4 features-box">
              <BsFillGeoAltFill className="icon" />
              <h3 className="features-title">Regional</h3>
              <hr className="ligne" />
              <p className="p-color">
                Nur Studenten der Otto-Friedrich-Universität Bamberg.
              </p>
            </div>

            <div className="col-lg-4 features-box">
              <BsCalendarWeekFill className="icon" />
              <h3 className="features-title">Aktivitäten</h3>
              <hr className="ligne" />
              <p className="p-color">
                Nehme an Veranstaltungen teil, um mehrere Personen auf einmal
                kennenzulernen.
              </p>
            </div>

            <div className="col-lg-4 features-box">
              <BsFillPeopleFill className="icon" />
              <h3 className="features-title">Gleichgesinnte</h3>
              <hr className="ligne" />
              <p className="p-color">
                Unser Matching-Algorithmus vermittelt Personen mit den gleichen
                Interessen.
              </p>
            </div>
          </div>
        </div>
      </section>

      <section className="colored-section" id="cta">
        <div className="cat-container">
          <h3 className="big-heading">
            Finde Freunde in der Weltkulturerbestadt...
          </h3>
          <Link
            to="/registierung"
            className=" download-butten btn btn-primary btn-lg "
            type="button"
          >
            Registrieren
          </Link>
          <Link
            className=" download-butten btn btn-outline-light btn-lg"
            type="button"
            to="/login"
          >
            Einloggen
          </Link>
        </div>
        <footer className="footer-box">
          <div className="footer-context">
            <p className="footer-text">
              Kontakt: bamberg-connect@uni-bamberg.de
            </p>
            <p className="footer-text">© Copyright BambergConnect</p>
            <div>
              <BsFacebook className="footer-icons" />
              <BsInstagram className="footer-icons" />
              <BsTwitter className="footer-icons" />
              <BsYoutube className="footer-icons" />
            </div>
          </div>
        </footer>
      </section>
    </div>
  );
}

export default NewLogin;
