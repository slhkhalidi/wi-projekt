const mongoose = require("mongoose");

//Buffer type is used when you usually work with items that get saved in binary form, a good example would be images.
const User = new mongoose.Schema(
  {
    vorname: { type: String, required: true },
    nachname: { type: String, required: true },
    geburtsDatum: { type: String, required: false },
    fakultaet: { type: String, required: true },
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true, unique: true },
    bild: { type: Buffer, required: false },
    interesse: { type: Array, required: true },
    abneigungen: { type: Array, required: true },
    publications: { type: Array, required: false },
    gesendeteAnfragen: { type: Array, required: false },
    receivedAnfragen: { type: Array, required: false },
    notifications: { type: Array, required: false },
    followers: { type: Array, required: false },
    role: { type: String, required: true },
    conversations: { type: Array, required: false },
  },
  { collection: "UniUsers" }
);

const userObject = mongoose.model("User", User);

module.exports = userObject;
